package nasabah
 
import (
    "context"
    "fmt"
    "database/sql"
    "errors"
    "github.com/crudgo/config"
    "github.com/crudgo/models"
    "log"
    "time"
)
 
const (
    table          = "nasabah"
    layoutDateTime = "2006-01-02 15:04:05"
)
 
// GetAll
func GetAll(ctx context.Context) ([]models.Nasabah, error) {
 
    var nasabahs []models.Nasabah
 
    db, err := config.MySQL()
 
    if err != nil {
        log.Fatal("Cant connect to MySQL", err)
    }
 
    queryText := fmt.Sprintf("SELECT * FROM %v ORDER BY id ASC", table)
 
    rowQuery, err := db.QueryContext(ctx, queryText)
 
    if err != nil {
        log.Fatal(err)
    }
 
    for rowQuery.Next() {
        var nasabah models.Nasabah
        var createdAt, updatedAt string
 
        if err = rowQuery.Scan(
			&nasabah.ID,
            &nasabah.Nama,
            &nasabah.Alamat,
            &nasabah.KodePos,
            &nasabah.NoHP,
            &nasabah.Email,
            &createdAt,
            &updatedAt); err != nil {
            return nil, err
        }
 
        //  Change format string to datetime for created_at and updated_at
        nasabah.CreatedAt, err = time.Parse(layoutDateTime, createdAt)
 
        if err != nil {
            log.Fatal(err)
        }
 
        nasabah.UpdatedAt, err = time.Parse(layoutDateTime, updatedAt)
 
        if err != nil {
            log.Fatal(err)
        }
 
        nasabahs = append(nasabahs, nasabah)
    }
 
    return nasabahs, nil
}

// inserrt Nasabah
func Insert(ctx context.Context, nsb models.Nasabah) error {
    db, err := config.MySQL()
 
    if err != nil {
        log.Fatal("Can't connect to MySQL", err)
    }
 
    queryText := fmt.Sprintf("INSERT INTO %v (nama, alamat, kodepos, no_hp, email, created_at, updated_at) values('%v','%v',%v,'%v','%v','%v','%v')", table,
        nsb.Nama,
        nsb.Alamat,
        nsb.KodePos,
        nsb.NoHP,
        nsb.Email,
        time.Now().Format(layoutDateTime),
        time.Now().Format(layoutDateTime))
 
    _, err = db.ExecContext(ctx, queryText)
 
    if err != nil {
        return err
    }
    return nil
}

// Update
func Update(ctx context.Context, nsb models.Nasabah) error {

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("UPDATE %v set nama ='%s', alamat ='%s', kodepos = %d, no_hp ='%s', email ='%s', updated_at = '%v' where id = '%d'",
		table,
		nsb.Nama,
		nsb.Alamat,
		nsb.KodePos,
		nsb.NoHP,
		nsb.Email,
		time.Now().Format(layoutDateTime),
		nsb.ID,
	)
	fmt.Println(queryText)

	_, err = db.ExecContext(ctx, queryText)

	if err != nil {
		return err
	}

	return nil
}

// Delete
func Delete(ctx context.Context, nsb models.Nasabah) error {

	db, err := config.MySQL()

	if err != nil {
		log.Fatal("Can't connect to MySQL", err)
	}

	queryText := fmt.Sprintf("DELETE FROM %v where id = '%d'", table, nsb.ID)

	s, err := db.ExecContext(ctx, queryText)

	if err != nil && err != sql.ErrNoRows {
		return err
	}

	check, err := s.RowsAffected()
	fmt.Println(check)
	if check == 0 {
		return errors.New("id tidak ada")
	}

	return nil
}
 