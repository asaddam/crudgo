package models
 
import (
    "time"
)
 
type (
    // Nasabah
    Nasabah struct {
        ID        int       `json:"id"`
        Nama      string    `name:"nama"`
        Alamat    string    `name:"alamat"`
        KodePos    int       `json:"kodepos"`
        NoHP  	  string      `name:"no_hp"`
        Email      string    `name:"email"`
        CreatedAt time.Time `json:"created_at"`
        UpdatedAt time.Time `json:"updated_at"`
    }
)
