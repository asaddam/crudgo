package main
 
import (
	"context"
    "log"
    "net/http"
    "github.com/crudgo/nasabah"
    "github.com/crudgo/models"
	"github.com/crudgo/utils"
	"encoding/json"
    "fmt"
    "strconv"
)

// GetNasabah
func GetNasabah(w http.ResponseWriter, r *http.Request) {
	if r.Method == "GET" {
		ctx, cancel := context.WithCancel(context.Background())

		defer cancel()

		nasabahs, err := nasabah.GetAll(ctx)

		if err != nil {
			fmt.Println(err)
		}

		utils.ResponseJSON(w, nasabahs, http.StatusOK)
		return
	}

	http.Error(w, "Tidak di ijinkan", http.StatusNotFound)
	return
}

// PostNasabah
func PostNasabah(w http.ResponseWriter, r *http.Request) {
    if r.Method == "POST" {
 
        if r.Header.Get("Content-Type") != "application/json" {
            http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
            return
        }
 
        ctx, cancel := context.WithCancel(context.Background())
        defer cancel()
 
        var nsb models.Nasabah
 
        if err := json.NewDecoder(r.Body).Decode(&nsb); err != nil {
            utils.ResponseJSON(w, err, http.StatusBadRequest)
            return
        }
 
        if err := nasabah.Insert(ctx, nsb); err != nil {
            utils.ResponseJSON(w, err, http.StatusInternalServerError)
            return
        }
 
        res := map[string]string{
            "status": "Succesfully",
        }
 
        utils.ResponseJSON(w, res, http.StatusCreated)
        return
	}
}

// UpdateNasabah
func UpdateNasabah(w http.ResponseWriter, r *http.Request) {
	if r.Method == "PUT" {

		if r.Header.Get("Content-Type") != "application/json" {
			http.Error(w, "Gunakan content type application / json", http.StatusBadRequest)
			return
		}

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var nsb models.Nasabah

		if err := json.NewDecoder(r.Body).Decode(&nsb); err != nil {
			utils.ResponseJSON(w, err, http.StatusBadRequest)
			return
		}

		fmt.Println(nsb)

		if err := nasabah.Update(ctx, nsb); err != nil {
			utils.ResponseJSON(w, err, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"status": "Succesfully",
		}

		utils.ResponseJSON(w, res, http.StatusCreated)
		return
	}

	http.Error(w, "Tidak di ijinkan", http.StatusMethodNotAllowed)
	return
}

// DeleteNasabah
func DeleteNasabah(w http.ResponseWriter, r *http.Request) {

	if r.Method == "DELETE" {

		ctx, cancel := context.WithCancel(context.Background())
		defer cancel()

		var nsb models.Nasabah

		id := r.URL.Query().Get("id")

		if id == "" {
			utils.ResponseJSON(w, "id tidak boleh kosong", http.StatusBadRequest)
			return
		}
		nsb.ID, _ = strconv.Atoi(id)

		if err := nasabah.Delete(ctx, nsb); err != nil {

			kesalahan := map[string]string{
				"error": fmt.Sprintf("%v", err),
			}

			utils.ResponseJSON(w, kesalahan, http.StatusInternalServerError)
			return
		}

		res := map[string]string{
			"status": "Succesfully",
		}

		utils.ResponseJSON(w, res, http.StatusOK)
		return
	}

	http.Error(w, "Tidak di ijinkan", http.StatusMethodNotAllowed)
	return
}

func main() {
	http.HandleFunc("/nasabah", GetNasabah)
	http.HandleFunc("/nasabah/create", PostNasabah)
	http.HandleFunc("/nasabah/update", UpdateNasabah)
	http.HandleFunc("/nasabah/delete", DeleteNasabah)
    fmt.Println("Success")
 
    err := http.ListenAndServe(":7000", nil)
 
    if err != nil {
        log.Fatal(err)
    }
}